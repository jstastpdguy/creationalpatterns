#pragma once
#include "Builder.h"
#include "Prototype.h"

class EquipmentStats;

/*
 * abstract interface for getting set weapon
 */
class ISetWeapon
{
public:
	virtual ~ISetWeapon(){};
	virtual EquipmentStats& GetWeapon(GearRarity& rarity) = 0;
};

/*
 * concrete class for getting mage set weapon
 */
class MageSetWeapon : public ISetWeapon
{
public:
	/*
	 * create director and builder for him, for making equipment stat
	 */
	EquipmentStats& GetWeapon(GearRarity& rarity) override
	{
		StatsDirector director;
		StatsBuilder builder;

		director.set_builder(&builder);

		GearType type = GearType::WEAPON;
		Stats stat("mage set");
		
		director.BuildEquipment(rarity, type, stat);
		return *builder.GetStats();
	}

};

/*
 * concrete class for getting warrior set weapon
 */
class WarriorSetWeapon : public ISetWeapon
{
public:
	/*
	 * create director and builder for him, for making equipment stat
	 */
	EquipmentStats& GetWeapon(GearRarity& rarity) override
	{
		StatsDirector director;
		StatsBuilder builder;

		director.set_builder(&builder);

		GearType type = GearType::WEAPON;
		Stats stat("warrior set");
		
		director.BuildEquipment(rarity, type, stat);
		return *builder.GetStats();
	}

};

/*
 * abstract interface for getting set armor
 */
class ISetArmor
{
public:
	virtual ~ISetArmor(){};
	virtual EquipmentStats& GetArmor(GearRarity& rarity) = 0;
};

/*
 * concrete class for getting mage set armor
 */
class MageSetArmor : public ISetArmor
{
public:
	/*
	 * create director and builder for him, for making equipment stat
	 */
	EquipmentStats& GetArmor(GearRarity& rarity) override
	{
		StatsDirector director;
		StatsBuilder builder;

		director.set_builder(&builder);

		GearType type = GearType::ARMOR;
		Stats stat("mage set");
		
		director.BuildEquipment(rarity, type, stat);
		return *builder.GetStats();
	}
};

/*
 * concrete class for getting warrior set armor
 */
class WarriorSetArmor : public ISetArmor
{
public:
	/*
	 * create director and builder for him, for making equipment stat
	 */
	EquipmentStats& GetArmor(GearRarity& rarity) override
	{		
		StatsDirector director;
		StatsBuilder builder;

		director.set_builder(&builder);

		GearType type = GearType::ARMOR;
		Stats stat("warrior set");
		
		director.BuildEquipment(rarity, type, stat);
		return *builder.GetStats();
	}
	
};

/*
 * abstract factory interface
 */
class IAbstractSetFactory
{
public:
	virtual ISetArmor * CreateSetArmor() const = 0;
	virtual ISetWeapon * CreateSetWeapon() const = 0;
	virtual ~IAbstractSetFactory(){};
};

/*
 * mage set factory make mage armor or weapon
 */
class MageSetFactory : public IAbstractSetFactory, public IGearPrototype
{
public:
	ISetArmor * CreateSetArmor() const override{
 
		return new MageSetArmor();
	}
	ISetWeapon * CreateSetWeapon() const override
	{
		return new MageSetWeapon();
	}

	IGearPrototype* clone() const override
	{
		return new MageSetFactory(*this);
	};
};

/*
 * warrior set factory make warrior armor or weapon
 */
class WarriorSetFactory : public IAbstractSetFactory, public IGearPrototype
{
public:
	ISetArmor * CreateSetArmor() const override{
		return new WarriorSetArmor();
	}
	ISetWeapon * CreateSetWeapon() const override
	{
		return new WarriorSetWeapon();
	}

	IGearPrototype* clone() const override
	{
		return new WarriorSetFactory(*this);
	};
};

/*
 * enumeration for for factory type classification
 */
enum ClassType
{
	mage = 0,
	warrior
};

/*
 * factory, which contains map with sets factories of all classes,
 * and clone them by operator []
 */
class SetsManufacturer
{
	std::map<ClassType, IGearPrototype*> set_fabrics;
public:
	SetsManufacturer()
	{
		set_fabrics[mage] = new MageSetFactory();
		set_fabrics[warrior] = new WarriorSetFactory();
	}

	IGearPrototype* operator[](const ClassType& type)
	{
		return set_fabrics[type]->clone();
	}

	~SetsManufacturer()
	{
		for( auto i : set_fabrics)
		{
			delete i.second;
		}
	}
};