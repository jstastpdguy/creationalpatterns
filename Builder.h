#pragma once
#include <ostream>
#include <string>
#include <vector>
#include "Singleton.h"

class StatsHolder;

enum class GearType
{
	ARMOR = 0,
	WEAPON,
	ACCESSORIES
};

//inline bool operator<(const GearType& lhs, const GearType& rhs)
//{
//	return static_cast<int>(lhs) < static_cast<int>(rhs);
//}

enum class GearRarity
{
	COMMON = 0,
	UNCOMMON,
	RARE,
	EPIC,
	UNICAL
};

/*
 * class contains one stats attribute
 */
class Stats
{
	std::string* str = nullptr;

public:

	/*
	 * constructors which allocate memory for string stat,
	 * and fill it, by value
	 */
	
	Stats(const std::string && str_)
	{
		str = new std::string(str_);
	}

	Stats(const std::string * str_)
	{
		str = new std::string(*str_);
	}

	/*
	 * copy constructor, which allocate new memory for new value
	 * and copy new value in variable
	 */
	
	Stats(const Stats& s)
	{	
		if(str)
		{
			delete str;
			str = nullptr;
		}

		str = new std::string(*s.str);
	}

	bool operator == (const Stats& other) const
	{
		return *str == *other.str;
	}
	
	/*
	 * operator << overloading
	 */
	
	friend std::ostream& operator <<(std::ostream& os, const Stats& s)
	{
		os << *s.str;
		return os;
	}

	/*
	 * free memory
	 */
	
	~Stats()
	{
		delete str;
	}
};

/*
 * class which store all stats for one gear
 */

class EquipmentStats
{
	std::vector<Stats> equipment_stats;
	GearRarity rarity_ = GearRarity::COMMON;
public:
	
	EquipmentStats& operator*()
	{
		return *this;
	} 

	Stats& operator[](const int i)
	{
		return equipment_stats[i];
	}

	/*return rarity of item by equipment_stats size*/
	std::string& GetRarity() const
	{
		std::string* result = nullptr;
		
		switch(rarity_)
		{
		case GearRarity::UNCOMMON:
			result = new std::string("UNCOMMON");
			break;
		case GearRarity::RARE:
			result = new std::string("RARE");
			break;
		case GearRarity::EPIC:
			result = new std::string("EPIC");
			break;
		case GearRarity::UNICAL:
			result = new std::string("UNICAL");
			break;
		default:
			break;
		}
		return *result;
	};

	void AddStat(const Stats& stat)
	{
		equipment_stats.push_back(stat);
		int old_rarity = static_cast<int>(rarity_);
		old_rarity++;
		rarity_ = static_cast<GearRarity>(old_rarity);
	};

	friend std::ostream& operator <<(std::ostream& os, const EquipmentStats& es)
	{
		os << es.GetRarity() << '\n';
		for (auto stat : es.equipment_stats)
		{
			os << stat << '\n';
		}
		
		return os;
	}
};

/*
 * builder interface, which represent all builder capabilities
 */

class IStatsBuilder
{
public:
	virtual ~IStatsBuilder(){};
	virtual void AddUncommonStat(GearType&) const = 0;
	virtual void AddRareStat(GearType&) const = 0;
	virtual void AddEpicStat(GearType&) const = 0;
	virtual void AddUnicalStat(GearType&) const = 0;
	virtual void AddUncommonStat(GearType&, Stats&) const = 0;
	virtual void AddRareStat(GearType&, Stats&) const = 0;
	virtual void AddEpicStat(GearType&, Stats&) const = 0;
	virtual void AddUnicalStat(GearType&, Stats&) const = 0;
};

/*
 * concrete builder, which implements all interface methods
 */
class StatsBuilder : public IStatsBuilder
{
	/*
	 * variable to store results of builder work
	 */
	EquipmentStats* stat_;

	/*
	 * singleton, which generate stats for item
	 */
	StatsHolder* StatsData = nullptr;

public:
	
	StatsBuilder()
	{
		/*get instance of stats holder*/
		StatsData = StatsHolder::GetInstance("stats_settings.json");
		/*allocate memory for new item*/
		reset();
	}

	~StatsBuilder()
	{
		delete stat_;
	}

	/*allocate memory for new empty item*/
	void reset()
	{
		stat_ = new EquipmentStats();
	}

	/*add one random uncommon stat from statsholder to stat_*/
	void AddUncommonStat(GearType& type) const override
	{
		GearRarity rarity = GearRarity::UNCOMMON;
		
		if(StatsData){
			stat_->AddStat(StatsData->GetStatRandom(rarity, type));
		}
	}

	/*add one random rare stat from statsholder to stat_*/
	void AddRareStat(GearType& type) const override
	{
		GearRarity rarity = GearRarity::RARE;
		if(StatsData){
			stat_->AddStat(StatsData->GetStatRandom(rarity, type));
		}
	};

	/*add one random epic stat from statsholder to stat_*/
	void AddEpicStat(GearType& type) const override
	{
		GearRarity rarity = GearRarity::EPIC;
		if(StatsData){
			stat_->AddStat(StatsData->GetStatRandom(rarity, type));
		}
	};

	/*add one random unical stat from statsholder to stat_*/
	void AddUnicalStat(GearType& type) const override
	{
		GearRarity rarity = GearRarity::UNICAL;
		if(StatsData){
			stat_->AddStat(StatsData->GetStatRandom(rarity, type));
		}
	};

	/*add one concrete uncommon stat from statsholder to stat_*/
	void AddUncommonStat(GearType& type, Stats& stat) const override
	{
		GearRarity rarity = GearRarity::UNCOMMON;

		if(StatsData){
			stat_->AddStat(StatsData->GetStatByName(rarity, type, stat));
		}
	};

	/*add one concrete rare stat from statsholder to stat_*/
	void AddRareStat(GearType& type, Stats& stat) const override
	{
		GearRarity rarity = GearRarity::RARE;

		if(StatsData){
			stat_->AddStat(StatsData->GetStatByName(rarity, type, stat));
		}
	};

	/*add one concrete epic stat from statsholder to stat_*/
	void AddEpicStat(GearType& type, Stats& stat) const override
	{
		GearRarity rarity = GearRarity::RARE;

		if(StatsData){
			stat_->AddStat(StatsData->GetStatByName(rarity, type, stat));
		}
	};

	/*add one concrete unical stat from statsholder to stat_*/
	void AddUnicalStat(GearType& type, Stats& stat) const override
	{
		GearRarity rarity = GearRarity::RARE;

		if(StatsData){
			stat_->AddStat(StatsData->GetStatByName(rarity, type, stat));
		}
	};
	
	/*returns finished object to client, and reset builder, allocated memory need to be free by client*/
	EquipmentStats* GetStats()
	{
		EquipmentStats* result = stat_;
		reset();
		return result;
	}
};

/* director, which build finished item, by builder, stored in there*/
class StatsDirector
{
	/*concrete builder, which would make item by steps*/
	IStatsBuilder* builder;

public:

	/*
	 * sets new builder to director's field
	 */
	
	void set_builder(IStatsBuilder* builder_)
	{
		this->builder = builder_;
	}

	/*
	 * build uncommon item with one uncommon stat by gear type
	 */
	
	void build_uncommon_equipment(GearType type)
	{
		this->builder->AddUncommonStat(type);
	}

	/*
	 * build rare item with two stats (uncommon and rare) by gear type
	 */
	
	void build_rare_equipment(GearType type)
	{
		this->builder->AddUncommonStat(type);
		this->builder->AddRareStat(type);
	}

	/*
	 * build rare item with three stats(uncommon, rare and epic) by gear type
	 */
	void build_epic_equipment(GearType type)
	{
		this->builder->AddUncommonStat(type);
		this->builder->AddRareStat(type);
		this->builder->AddEpicStat(type);
	}

	/*
	 * build rare item with four stats(uncommon, rare, epic and unical) by gear type
	 */
	void build_unical_equipment(GearType type)
	{
		this->builder->AddUncommonStat(type);
		this->builder->AddRareStat(type);
		this->builder->AddEpicStat(type);
		this->builder->AddUnicalStat(type);
	}

	void build_uncommon_equipment(GearType type, Stats& stat)
	{
		this->builder->AddUncommonStat(type, stat);
	}

	void build_rare_equipment(GearType type, Stats& stat)
	{
		this->builder->AddUncommonStat(type, stat);
		this->builder->AddRareStat(type, stat);
	}

	void build_epic_equipment(GearType type, Stats& stat)
	{
		this->builder->AddUncommonStat(type, stat);
		this->builder->AddRareStat(type, stat);
		this->builder->AddEpicStat(type, stat);
	}

	void build_unical_equipment(GearType type, Stats& stat)
	{
		this->builder->AddUncommonStat(type, stat);
		this->builder->AddRareStat(type, stat);
		this->builder->AddEpicStat(type, stat);
		this->builder->AddUnicalStat(type, stat);
	}
	
	/*
	 *  call build function corresponding rarity
	 */
	void BuildEquipment(GearRarity& rarity, GearType& type)
	{
		switch(rarity)
		{
		case GearRarity::UNCOMMON:
			build_uncommon_equipment(type);
			break;
		case GearRarity::RARE:
			build_rare_equipment(type);
			break;
		case GearRarity::EPIC:
			build_epic_equipment(type);
			break;
		case GearRarity::UNICAL:
			build_unical_equipment(type);
			break;
		default:
			break;
		}
	}

	/*
	 *  call build function corresponding rarity and stat value
	 */
	void BuildEquipment(GearRarity& rarity, GearType& type, Stats& stat)
	{
		switch(rarity)
		{
		case GearRarity::UNCOMMON:
			build_uncommon_equipment(type, stat);
			break;
		case GearRarity::RARE:
			build_rare_equipment(type, stat);
			break;
		case GearRarity::EPIC:
			build_epic_equipment(type, stat);
			break;
		case GearRarity::UNICAL:
			build_unical_equipment(type, stat);
			break;
		default:
			break;
		}
	}
};