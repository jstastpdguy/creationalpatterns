#pragma once

enum class GearType;

/*
 * realisation of this interface in abstract factory
 */
class IGearPrototype
{
public:

	virtual IGearPrototype* clone() const = 0;
	virtual ~IGearPrototype() {};
};