#include "Singleton.h"
#include <fstream>
#include <iostream>
#include <vector>
#include "Builder.h"

StatsHolder* StatsHolder::holder_ = nullptr;

void StatsHolder::init_instance()
{
	//buffer for reading from file
	char* read_buffer = new char[64];
	read_buffer[63] = '\0';

	//storage buffers
	std::string * rarity = nullptr;
	std::string * equip_type = nullptr;
	Stats * stat = nullptr;

	std::map<GearType, std::vector<Stats>>* gear_possible_stats = new std::map<GearType, std::vector<Stats>>();
	std::vector<Stats>* v = new std::vector<Stats>;

	//flag to check what was get from read_buffer
	bool is_word = false;
	//level of nesting is json file
	uint8_t level= 0;
	
	while(file->getline(read_buffer, 63, '"') || !file->eof())
	{
		/*flag switches on every read from file, and represent that contains buffer
		 * string or other symbols
		 */
		is_word = !is_word;

		/*
		 * if buffer contains string check level of nesting,
		 * to choose which variable should be written level 1 - rarity, level 2 - gear type,
		 * level 3 - stat
		 */
		if(!is_word){

			switch(level)
			{
			case 1:

				if(rarity)
				{
					delete rarity;
				}
				rarity = new std::string(read_buffer);

				
				break;
			case 2:
				if(equip_type)
				{
					delete equip_type;
				}
				equip_type = new std::string(read_buffer);

				break;
			case 3:
				if(stat)
				{
					delete stat;
				}
				stat = new Stats(read_buffer);
				v->push_back(*stat);

				break;
			default:
				break;
			}
			
		}else
		{
			/*
			 * if buffer doesn't contain word, check it for open/close
			 * brackets to change level of nesting
			 */
			
			for(int i = 0; read_buffer[i] != '\0'; i++)
			{
				if (read_buffer[i] == ']'){
					level--;

					/*
					 * if level goes down, check which level is closed
					 */
					
					if(level == 2)
					{
						/*gear type level closed - next string would be rarity level, so ve need make or fill map<gear type, vector>*/
						if(!gear_possible_stats)
						{
							gear_possible_stats = new std::map<GearType, std::vector<Stats>>();
						}

						GearType type;
						
						if(*equip_type == "weapon")
							type = GearType::WEAPON;
						else if (*equip_type == "armor")
							type = GearType::ARMOR;
						
						gear_possible_stats->insert(std::pair<GearType, std::vector<Stats>>(type, *v));
						v->clear();
					}else if( level == 1)
					{
						/*rarity level closed - next string would be default level, so we insert existing gear map to main map*/
						if(!stats_)
						{
							stats_ = new std::map<GearRarity, std::map<GearType, std::vector<Stats>>>();
						}

						GearRarity rarity_gear;
						
						if(*rarity == "uncommon")
							rarity_gear = GearRarity::UNCOMMON;
						else if(*rarity == "rare")
							rarity_gear = GearRarity::RARE;
						else if(*rarity == "epic")
							rarity_gear = GearRarity::EPIC;
						else if(*rarity == "unic")
							rarity_gear = GearRarity::UNICAL;
						
						stats_->insert(std::pair<GearRarity, std::map<GearType, std::vector<Stats>>>(rarity_gear, *gear_possible_stats));
						gear_possible_stats->clear();
					}
				}else if(read_buffer[i] == '[') level++;
			}
		}
	}

	file->close();

	//free memory
	delete [] read_buffer;
	delete rarity;
	delete equip_type;
	delete stat;
	delete gear_possible_stats;
	delete v;
}

Stats& StatsHolder::GetStatRandom(GearRarity& rarity, GearType& gear_type) const
{
	srand ( time(NULL) );
	//get size of vector, of stored by keys rarity and gear_type
	int8_t StatsCount = stats_->at(rarity).at(gear_type).size();
	//get random stat from vectors, given by keys
	int8_t index_to_get = rand()%StatsCount;
	return stats_->at(rarity).at(gear_type).at(index_to_get);
}

Stats& StatsHolder::GetStatByName(GearRarity& rarity, GearType& gear_type, Stats& stat) const
{
	Stats* result = nullptr;
	//copy vector from map by key
	std::vector<Stats> stats = stats_->at(rarity).at(gear_type);

	//find stat in vector and return it
	for( auto value : stats)
	{
		if(value == stat)
		{
			result = new Stats(value);
			return *result;
		}
	}

	//return nullptr if stat wasn't found
	return *result;
}


StatsHolder* StatsHolder::GetInstance(std::string&& config_filename)
{
	if (StatsHolder::holder_ == nullptr)
	{
		//if singleton doesn't initialized, create instance of it and init
		holder_ = new StatsHolder(config_filename);
		holder_->init_instance();
	}
	
	return holder_;
}
