#pragma once
#include <map>
#include <string>
#include <fstream>
#include <vector>

enum class GearType;
enum class GearRarity;

class Stats;

//class grants access to equipment stats database
class StatsHolder
{
	//pointer to database file, which initialize, when class initialize, and closes after that
	std::ifstream* file = nullptr;
	
protected:

	/*constructors of this class cannot be called from outside of class, because
	 * we don't need to make copies of this class, only one instance
	 */
	
	
	//constructor, which gets ref to name of database file and open it
	StatsHolder(std::string & config_filename)
	{
		file = new std::ifstream(config_filename, std::ifstream::binary);
		if(!file->is_open()) throw;
	};

	//free memory
	~StatsHolder()
	{
		delete file;
	}

	//parse database file and fill "stats_" filed which contains all info about stats
	void init_instance();

	//map, which contains all parsed info from database file
	std::map<GearRarity, std::map<GearType, std::vector<Stats>>>* stats_ = nullptr;

	//static pointer to instance of singleton
	static StatsHolder* holder_;

public:

	/*
	 * this class shouldn't be copied or assignable
	 */
	
	StatsHolder(StatsHolder &) = delete;
	void operator=(const StatsHolder &) = delete;

	/*
	 * returns value from map, filled from database
	 */
	Stats& GetStatRandom(GearRarity& rarity, GearType& gear_type) const;

	/*
	 * search value in stats vector and returns
	 */
	Stats& GetStatByName(GearRarity& rarity, GearType& gear_type, Stats& stat) const;

	/*
	 * at first time creates instance of this class and put ref to it in static field, after thar init stats map,
	 * in other way returns ref, which stored in static field
	 */
	static StatsHolder* GetInstance(std::string && config_filename);
};