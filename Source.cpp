#include <iostream>

#include "AbstractFactory.h"
#include "Builder.h"

int main()
{

	StatsDirector* director = new StatsDirector();
	StatsBuilder* builder = new StatsBuilder();

	Stats mage_set("mage set");
	
	director->set_builder(builder);
	director->build_rare_equipment(GearType::WEAPON);

	EquipmentStats* s = builder->GetStats();

	std::cout << *s << std::endl;
	delete s;
	
	director->build_unical_equipment(GearType::ARMOR);
	s = builder->GetStats();

	std::cout << *s << std::endl;
	delete s;

	director->build_uncommon_equipment(GearType::WEAPON);
	s = builder->GetStats();

	std::cout << *s << std::endl;
	delete s;

	director->build_epic_equipment(GearType::ARMOR);
	s = builder->GetStats();

	std::cout << *s << std::endl;
	delete s;

	SetsManufacturer sm;
	MageSetFactory* mf = dynamic_cast<MageSetFactory*>(sm[ClassType::mage]->clone());
	WarriorSetFactory* wf = dynamic_cast<WarriorSetFactory*>(sm[ClassType::warrior]->clone());
	GearRarity rarity = GearRarity::UNICAL;
	
	std::cout << mf->CreateSetArmor()->GetArmor(rarity) << '\n';
	std::cout << wf->CreateSetArmor()->GetArmor(rarity) << '\n';

	delete director;
	delete builder;
	delete mf;
	delete wf;
		
	return 0;
}